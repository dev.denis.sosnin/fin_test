document.addEventListener("DOMContentLoaded", function(){
    orderBook = new OrderBook({
        tablesScrollContainer : '.js-orderBook__tables',
        asksTableSelector : '.js-orderBook__asks',
        bidsTableSelector : '.js-orderBook__bids',
        lazySize : 100,
        dataJsonLink : '1.json'
    })
})

const graph = {
    GREEN:{
        angle: '270deg',
        color: '#dcf4e7'
    },
    RED:{
        angle: '90deg',
        color: '#fbe7e8'
    }
}

class OrderBook{
    constructor({tablesScrollContainer, asksTableSelector, bidsTableSelector, lazySize, dataJsonLink}){

        this.bidsTable = document.querySelector(bidsTableSelector).getElementsByTagName('tbody')[0]
        this.asksTable = document.querySelector(asksTableSelector).getElementsByTagName('tbody')[0]
        this.scrollContainer = document.querySelector(tablesScrollContainer)

        this.cursor = 1
        this.size = lazySize

        this.endAsksTotal
        this.endBidsTotal

        this.dataJsonLink = dataJsonLink

        let data
        let loaded = false
        let solved = false

        this.init()
        this.bindEvents()
    }

    async init(){

        // load Json
        await fetch(this.dataJsonLink)
            .then(responce => responce.json())
            .then(data => {
                this.data = data
                this.loaded = true
            })

        // calc Total
        this.endBidsTotal = this.data.bids.reduce((total, el)=>{
            el.total = total + el.quantity
            return el.total
        }, 0);

        this.endAsksTotal = this.data.asks.reduce((total, el)=>{
            el.total = total + el.quantity
            return el.total
        }, 0);

        this.solved = true

        // render first @size elements
        this.addItems(this.data.bids, this.bidsItemView, this.bidsTable, this.endBidsTotal, graph.GREEN)
        this.addItems(this.data.asks, this.asksItemView, this.asksTable, this.endAsksTotal, graph.RED)

        this.cursor++
    }

    bidsItemView(el){
        let html = ''
        html += `<td>${el.numberOfOrders}</td>`
        html += `<td>${el.total}</td>`
        html += `<td>${el.quantity}</td>`
        html += `<td>${el.price}</td>`
        return html
    }

    asksItemView(el){
        let html = ''
        html += `<td>${el.price}</td>`
        html += `<td>${el.quantity}</td>`
        html += `<td>${el.total}</td>`
        html += `<td>${el.numberOfOrders}</td>`    
        return html
    }

    addItems(data, view, table, endTotal, graph){
        return data.slice((this.cursor - 1) * this.size, this.cursor * this.size)
            .map( (el) => {

                let tr = document.createElement('tr')
                tr.innerHTML = view(el)

                let offset = el.total / endTotal * 100

                //min offset
                if(offset < 1) offset = 1
                tr.style.background = `linear-gradient(${graph.angle}, ${graph.color} ${offset}%, #fff ${offset}%)`
                
                table.appendChild(tr)
            });
    }

    bindEvents(){
        this.scrollContainer.onscroll = (e) => {
            if (e.target.scrollHeight - this.scrollContainer.offsetHeight - e.target.scrollTop < 400 ) {
                this.bidsLastTotal = this.addItems(this.data.bids, this.bidsItemView, this.bidsTable, this.endBidsTotal, graph.GREEN)
                this.asksLastTotal = this.addItems(this.data.asks, this.asksItemView, this.asksTable, this.endAsksTotal, graph.RED)

                this.cursor++
            }
        }
    }
}